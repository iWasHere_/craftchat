package iwas.scrapchat.struct;

import iwas.scrapchat.ScrapChat;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class ChatMessage {

    private String message;
    private ChatUser user;
    private String room;
    private boolean action;

    public ChatMessage(String message, ChatUser user, String room, boolean action) {
        this.message = message;
        this.user = user;
        this.room = room;
        this.action = action;
    }

    public ITextComponent getFormattedMessage() {
        TextComponentString userComponent;

        TextComponentString roomComponent;

        // Standard mode
        if (!ScrapChat.chat.isSingleRoomMode()) {
            roomComponent = new TextComponentString("[" + room + "] ");
            roomComponent.setStyle(new Style().setColor(TextFormatting.YELLOW));
        } else { // Single room mode (hide room names)
            roomComponent = new TextComponentString("");
        }

        TextComponentString messageComponent = new TextComponentString(message);
        messageComponent.setStyle(new Style().setColor(TextFormatting.WHITE));

        if (!action) { // Standard messages
            userComponent = new TextComponentString("<" + user.getUsername() + "> ");
            userComponent.setStyle(new Style().setColor(TextFormatting.GOLD));
        } else { // /me messages
            userComponent = new TextComponentString("* " + user.getUsername() + " ");
            userComponent.setStyle(new Style().setColor(TextFormatting.GOLD).setItalic(true));
            messageComponent.setStyle(new Style().setColor(TextFormatting.WHITE).setItalic(true));
        }

        return roomComponent.appendSibling(userComponent.appendSibling(messageComponent));
    }

}
