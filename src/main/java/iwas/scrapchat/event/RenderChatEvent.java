package iwas.scrapchat.event;

import iwas.scrapchat.ScrapChat;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderTooltipEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.config.GuiUtils;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;

@Mod.EventBusSubscriber(Side.CLIENT)
public class RenderChatEvent {

    private static List<Button> buttons;
    private static int lastWidth;
    private static int lastHeight;

    public static void updateButtons(List<String> withRooms) {
        if (buttons == null) buttons = new ArrayList<>();
        buttons.clear();

        ScaledResolution scaledResolution = new ScaledResolution(Minecraft.getMinecraft());
        int width = scaledResolution.getScaledWidth();
        int height = scaledResolution.getScaledHeight();

        // These are polled on frame to determine if we need to reupdate
        lastWidth = width;
        lastHeight = height;

        // Create SRM button
        int color = ScrapChat.chat.isSingleRoomMode() ? Color.green.getRGB() : Color.red.getRGB();
        Button srmButton = new Button("", "Single Room", Result.SINGLE_ROOM_MODE, color);
        srmButton.setPosition(width - 64, 16, 64, 8);
        buttons.add(srmButton);

        // Create Minecraft Chat button
        color = ScrapChat.chat.isActive() ? Color.white.getRGB() : Color.green.getRGB();
        Button minecraftButton = new Button("", "Minecraft", Result.DISABLE_SCRAP_CHAT, color);
        minecraftButton.setPosition(width - 64, 64, 64, 8);
        buttons.add(minecraftButton);

        // Add room buttons
        for (byte i = 0; i < withRooms.size(); i++) {
            String room = withRooms.get(i);

            color = ScrapChat.chat.getCurrentRoom().equals(room) ? Color.green.getRGB() : Color.white.getRGB();
            if (!ScrapChat.chat.isActive()) color = Color.white.getRGB();

            Button roomButton = new Button(room, room, Result.SWITCH_ROOM, color);
            roomButton.setPosition(width - 64, 86 + (i * 16), 64, 8);

            buttons.add(roomButton);
        }
    }

    public static void updateButtons() {
        updateButtons(ScrapChat.chat.getInRooms());
    }

    @SubscribeEvent
    public void renderChat(RenderGameOverlayEvent.Chat event) {
        if (!ScrapChat.chat.isConnected()) return; // Return if we aren't connected
        if (!(Minecraft.getMinecraft().currentScreen instanceof GuiChat)) return; // Render on GuiChat
        if (buttons == null) updateButtons(); // Initially create buttons if needed

        ScaledResolution scaledResolution = new ScaledResolution(Minecraft.getMinecraft());
        int width = scaledResolution.getScaledWidth();
        int height = scaledResolution.getScaledHeight();

        if (width != lastWidth || height != lastHeight) updateButtons(); // Reupdate on resolution change

        int mouseX = Mouse.getX() / scaledResolution.getScaleFactor();
        int mouseY = Math.abs(Mouse.getY() - Minecraft.getMinecraft().displayHeight) / scaledResolution.getScaleFactor();

        // Draw buttons
        for (byte i = 0; i < buttons.size(); i++) {
            Button button = buttons.get(i);

            // Determine color
            int drawColor = button.color;
            if (button.hovered(mouseX, mouseY)) drawColor = Color.yellow.getRGB();

            button.draw(drawColor); // Draw
        }
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST, receiveCanceled = true)
    public void mouseInput(GuiScreenEvent.MouseInputEvent.Pre event) {
        if (!ScrapChat.chat.isConnected()) return; // Return if we aren't connected
        if (!(Minecraft.getMinecraft().currentScreen instanceof GuiChat)) return; // Render on GuiChat

        // Take inputs
        ScaledResolution scaledResolution = new ScaledResolution(Minecraft.getMinecraft());
        int mouseX = Mouse.getX() / scaledResolution.getScaleFactor();
        int mouseY = Math.abs(Mouse.getY() - Minecraft.getMinecraft().displayHeight) / scaledResolution.getScaleFactor();

        boolean leftClick = (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()); // Left Click input

        // Get hovered buttons
        Button hovered = null;
        for (Button button : buttons) {
            if (button.hovered(mouseX, mouseY)) {
                hovered = button;
                break;
            }
        }

        if (leftClick && hovered != null) { // Left click
            if (hovered.buttonType.equals(Result.DISABLE_SCRAP_CHAT)) { // Disable Scrap Chat
                if (ScrapChat.chat.isActive()) {
                    ScrapChat.chat.setActive(false);
                    updateButtons(); // Update
                }
            }
            if (hovered.buttonType.equals(Result.SWITCH_ROOM)) { // Room Switch
                if (ScrapChat.chat.isActive() && ScrapChat.chat.getCurrentRoom().equals(hovered.roomResult)) return; // Don't switch to same room
                if (!ScrapChat.chat.isActive()) ScrapChat.chat.setActive(true); // Enable Scrap Chat if need be
                ScrapChat.chat.switchRooms(hovered.roomResult); // Switch room
                updateButtons(); // Update
            }
            if (hovered.buttonType.equals(Result.SINGLE_ROOM_MODE)) { // SRM
                ScrapChat.chat.toggleSingleRoomMode(); // Toggle SRM
                updateButtons(); // Update
            }
        }
    }

    public static class Button {

        private String roomResult;
        private String displayed;
        private Result buttonType;
        private int color;
        private int top;
        private int left;
        private int width;
        private int height;

        public Button(String roomResult, String displayed, Result buttonType, int color) {
            this.roomResult = roomResult;
            this.displayed = displayed;
            this.buttonType = buttonType;
            this.color = color;

            top = 0;
            left = 0;
            width = 0;
            height = 0;
        }

        public void draw(int withColor) {
            drawTooltipStyle(displayed, left, top, withColor);
        }

        public void setPosition(int left, int top, int width, int height) {
            this.top = top;
            this.left = left;
            this.width = width;
            this.height = height;
        }

        public boolean hovered(int mouseX, int mouseY) {
            return (mouseX > left && mouseX < left + width && mouseY > top && mouseY < top + height);
        }

        public String getRoomResult() {
            return roomResult;
        }

        public String getDisplayed() {
            return displayed;
        }

        public Result getButtonType() {
            return buttonType;
        }

        public int getColor() {
            return color;
        }

        // Stolen from GuiUtils#drawHoveringText
        private void drawTooltipStyle(String string, int x, int y, int color) {
            int tooltipX = x;
            int tooltipY = y;

            int tooltipTextWidth = width;
            int tooltipHeight = 8;
            
            final int zLevel = 300;
            int backgroundColor = 0xFF100010;
            //int borderColorStart = 0xFF5000FF;
            //int borderColorEnd = (borderColorStart & 0xFEFEFE) >> 1 | borderColorStart & 0xFF000000;
            //int borderColorEnd = 0xFF5000FF;
            int borderColorStart = color;
            int borderColorEnd = (borderColorStart & 0xFEFEFE) >> 1 | borderColorStart & 0xFF303030;

            GlStateManager.disableRescaleNormal();
            RenderHelper.disableStandardItemLighting();
            GlStateManager.disableLighting();
            GlStateManager.disableDepth();

            // Draw background
            GuiUtils.drawGradientRect(zLevel, tooltipX - 3, tooltipY - 4, tooltipX + tooltipTextWidth + 3, tooltipY - 3, backgroundColor, backgroundColor);
            GuiUtils.drawGradientRect(zLevel, tooltipX - 3, tooltipY + tooltipHeight + 3, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 4, backgroundColor, backgroundColor);
            GuiUtils.drawGradientRect(zLevel, tooltipX - 3, tooltipY - 3, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 3, backgroundColor, backgroundColor);
            GuiUtils.drawGradientRect(zLevel, tooltipX - 4, tooltipY - 3, tooltipX - 3, tooltipY + tooltipHeight + 3, backgroundColor, backgroundColor);
            GuiUtils.drawGradientRect(zLevel, tooltipX + tooltipTextWidth + 3, tooltipY - 3, tooltipX + tooltipTextWidth + 4, tooltipY + tooltipHeight + 3, backgroundColor, backgroundColor);
            GuiUtils.drawGradientRect(zLevel, tooltipX - 3, tooltipY - 3 + 1, tooltipX - 3 + 1, tooltipY + tooltipHeight + 3 - 1, borderColorStart, borderColorEnd);
            GuiUtils.drawGradientRect(zLevel, tooltipX + tooltipTextWidth + 2, tooltipY - 3 + 1, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 3 - 1, borderColorStart, borderColorEnd);
            GuiUtils.drawGradientRect(zLevel, tooltipX - 3, tooltipY - 3, tooltipX + tooltipTextWidth + 3, tooltipY - 3 + 1, borderColorStart, borderColorStart);
            Gui.drawRect(tooltipX - 3, tooltipY + tooltipHeight + 2, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 3, borderColorEnd);

            // Draw Text
            Minecraft.getMinecraft().fontRenderer.drawStringWithShadow(string, tooltipX, tooltipY, color);
        }

    }

    private enum Result {
        SWITCH_ROOM,
        DISABLE_SCRAP_CHAT,
        SINGLE_ROOM_MODE
    }

}
