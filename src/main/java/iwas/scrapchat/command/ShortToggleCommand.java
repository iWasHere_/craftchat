package iwas.scrapchat.command;

import iwas.scrapchat.ScrapChat;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;

public class ShortToggleCommand extends CommandBase {

    @Override
    public String getName() {
        return "st";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/st";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length != 0) ScrapChat.chat.chatString(getUsage(sender));

        ScrapChat.chat.toggleActive();
    }

}
