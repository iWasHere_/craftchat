package iwas.scrapchat.struct;

import iwas.scrapchat.ScrapChat;
import org.json.JSONObject;

public class ChatUser {

    private String username;
    private int id;

    public ChatUser(JSONObject object) {
        try {
            this.username = object.getString("username").replace("&lt;", "<").replace("&gt;", ">");
            this.id = object.getInt("id");
        }
        catch (Exception e) {
            ScrapChat.logger.error("Error creating user:\n" + e.getLocalizedMessage());
        }
    }

    public String getUsername() { return username; }

    public int getId() { return id; }

}
