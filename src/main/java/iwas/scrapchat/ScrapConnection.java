package iwas.scrapchat;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import iwas.scrapchat.event.RenderChatEvent;
import iwas.scrapchat.struct.ChatMessage;
import iwas.scrapchat.struct.ChatUser;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.event.RenderLivingEvent;
import org.json.JSONObject;

import java.util.*;

public class ScrapConnection{

    private Socket socket;
    private Map<Integer, ChatUser> userMap;
    private List<String> inRooms;
    private String currentRoom;
    private boolean connected;
    private boolean singleRoomMode;
    private boolean active;

    public ScrapConnection() {
        userMap = new HashMap<>();
        inRooms = new ArrayList<>();
        currentRoom = "";
        connected = false;
        singleRoomMode = false;
        active = false;
    }

    public void start() {
        if (active) return; // Don't create a second thread

        ScrapChat.logger.info("Starting chat thread...");

        Connector connector = new Connector();
        Thread newThread = new Thread(connector);
        newThread.start();
    }

    public void end() {
        ScrapChat.logger.info("Ending chat thread...");

        socket.disconnect();
        userMap.clear();
        inRooms.clear();
        currentRoom = "";
        connected = false;
    }

    private void onUpdate() {
        RenderChatEvent.updateButtons(inRooms); // Update chat UI buttons
    }

    public void joinRoom(String room) {
        try {
            JSONObject object = new JSONObject();
            object.put("room", room);
            socket.emit("join room", object);
        }
        catch (Exception e) {
            ScrapChat.logger.error("Error joining room:\n" + e.getLocalizedMessage() );
        }
    }

    public void leaveRoom(String room) {
        try {
            socket.emit("leave room", room);
        }
        catch (Exception e) {
            ScrapChat.logger.error("Error leaving room:\n" + e.getLocalizedMessage());
        }
    }

    public void switchRooms(String room) {
        if (inRooms.contains(room)) { // Success
            currentRoom = room;
            chatString(I18n.format("gui.scrapchat.switch_room", room), new Style().setColor(TextFormatting.LIGHT_PURPLE));
            onUpdate();
        } else { // Failure
            chatString(I18n.format("gui.scrapchat.not_in_room", room), new Style().setColor(TextFormatting.LIGHT_PURPLE));
        }
    }

    public void sendMessage(String message) {
        sendMessage(currentRoom, message);
    }

    public void sendMessage(String room, String message) {
        try {

            // Action checks
            boolean action = false;

            if (message.startsWith("//")) {
                action = true;
                message = message.replaceFirst("//", "");
            }
            if (message.startsWith("*") && message.endsWith("*")) {
                action = true;
                message = message.substring(1, message.length() - 1);
            }
            if (message.startsWith("/me ")) {
                action = true;
                message = message.replaceFirst("/me", "");
            }

            // Build & send
            JSONObject object = new JSONObject();
            object.put("message", message);
            object.put("room", room);

            socket.emit((!action) ? "message" : "action", object);

            if (Config.showInGame) setDescription("Playing Minecraft"); // Say we're playing Minecraft (Doesn't work on connect)
        }
        catch (Exception e) {
            ScrapChat.logger.error("Error sending message:\n" + e.getLocalizedMessage());
        }
    }

    public void setDescription(String description) {
        socket.emit("set game", description);
    }

    public boolean isConnected() { return connected; }

    public void toggleSingleRoomMode() {
        singleRoomMode = !singleRoomMode;
        String text = I18n.format("gui.scrapchat.single_room_mode", I18n.format((singleRoomMode) ? "gui.scrapchat.on" : "gui.scrapchat.off"));
        chatString(text, new Style().setColor(TextFormatting.LIGHT_PURPLE));
        onUpdate();
    }

    public boolean isSingleRoomMode() { return singleRoomMode; }

    public void toggleActive() {
        setActive(!isActive());
    }

    public void setActive(boolean active) {
        this.active = active;
        String text = I18n.format("gui.scrapchat.active_toggle", I18n.format((active) ? "gui.scrapchat.scrapchat" : "gui.scrapchat.minecraftchat"));
        chatString(text, new Style().setColor(TextFormatting.LIGHT_PURPLE));
        onUpdate();
    }

    public boolean isActive() { return active; }

    public static void chatString(ITextComponent component) {
        Minecraft.getMinecraft().player.sendMessage(component);
    }

    public static void chatString(String message, Style style) {
        TextComponentString componentString = new TextComponentString(message);
        componentString.setStyle(style);
        chatString(componentString);
    }

    public static void chatString(String message) {
        chatString(message, new Style());
    }

    public static void scrapMessage(String message, Style style) {
        TextComponentString componentString = new TextComponentString(message);
        componentString.setStyle(style);
        chatString(componentString);
    }

    public static void scrapMessage(ITextComponent component) {
        // When in SRM & using MC Chat, suppress messages
        if (!ScrapChat.chat.active && ScrapChat.chat.isSingleRoomMode()) return;

        // Show message
        chatString(component);
    }

    public String getCurrentRoom() {
        return currentRoom;
    }

    public List<String> getInRooms() {
        return inRooms;
    }

    private class Connector implements Runnable {

        @Override
        public void run() {
            IO.Options options = new IO.Options();
            options.query = String.format("token=%s" + "&id=%s", Config.token, "forge_scrap_chat");
            options.transports = new String[] {"websocket"};
            options.reconnection = true;
            options.timeout = 60000;

            try {
                ScrapChat.logger.info("Attempting connection...");
                socket = IO.socket("https://chat.scrap.tf", options);
                bindSocketHandlers();
                socket.connect();
            }
            catch (Exception e) {
                ScrapChat.logger.error("Error connecting:\n" + e.getLocalizedMessage());
            }
        }

        private void bindSocketHandlers() {
            socket.on(Socket.EVENT_CONNECT, onConnect);
            socket.on(Socket.EVENT_DISCONNECT, onDisconnect);
            socket.on("message", onMessage);
            socket.on("users", onUser);
            socket.on("joined room", onRoomJoined);
            socket.on("left room", onRoomLeft);
            socket.on("user joined room", onUserJoinedRoom);
            socket.on("user left room", onUserLeftRoom);
            socket.on("user joined", onUserJoined);
            socket.on("user left", onUserLeft);
            socket.on("users update", onUserUpdate);
            socket.on("server", onServer);
        }

        private Emitter.Listener onConnect = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                ScrapChat.logger.info("Connected to Scrap Chat");
                chatString(I18n.format("gui.scrapchat.connected"), new Style().setColor(TextFormatting.GRAY).setItalic(true));
                connected = true;
                setActive(true);
            }
        };

        private Emitter.Listener onDisconnect = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                ScrapChat.logger.info("Disconnected from Scrap Chat");
                chatString(I18n.format("gui.scrapchat.disconnected"), new Style().setColor(TextFormatting.GRAY).setItalic(true));
                connected = false;
                setActive(false);
            }
        };

        private Emitter.Listener onUser = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject object = (JSONObject) args[0];
                    userMap.clear(); // Clear UserMap

                    // Begin to repopulate it
                    Iterator keys = object.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        JSONObject userObject = object.getJSONObject(key);

                        userMap.put(Integer.parseInt(key), new ChatUser(userObject)); // Add user
                    }
                }
                catch (Exception e) {
                    ScrapChat.logger.error("Error updating users:\n" + e.getLocalizedMessage());
                }
            }
        };

        private Emitter.Listener onRoomJoined = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject object = (JSONObject) args[0];

                    String room = object.getString("room");
                    currentRoom = room;
                    inRooms.add(room);
                    scrapMessage(I18n.format("gui.scrapchat.join_room", room),  new Style().setColor(TextFormatting.GRAY).setItalic(true));
                    onUpdate();
                }
                catch (Exception e) {
                    ScrapChat.logger.error("Error joining room:\n" + e.getLocalizedMessage());
                }
            }
        };

        private Emitter.Listener onRoomLeft = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    String room = (String) args[0];
                    inRooms.remove(room);
                    scrapMessage(I18n.format("gui.scrapchat.left_room", room),  new Style().setColor(TextFormatting.GRAY).setItalic(true));
                    onUpdate();
                }
                catch (Exception e) {
                    ScrapChat.logger.error("Error leaving room:\n" + e.getLocalizedMessage());
                }
            }
        };

        private Emitter.Listener onUserJoinedRoom = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject object = (JSONObject) args[0];
                    ChatUser user = object.has("user") ? new ChatUser(object.getJSONObject("user")) : userMap.get(object.getInt("userid"));
                    String room = object.getString("room");

                    if (user == null) return;

                    String displayedMessage = I18n.format("gui.scrapchat.user_join_room", user.getUsername(), room);
                    scrapMessage(displayedMessage,  new Style().setColor(TextFormatting.GRAY).setItalic(true));
                }
                catch (Exception e) {
                    ScrapChat.logger.error("Error on user joining room:\n" + e.getLocalizedMessage());
                }
            }
        };

        private Emitter.Listener onUserLeftRoom = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject object = (JSONObject) args[0];
                    ChatUser user = object.has("user") ? new ChatUser(object.getJSONObject("user")) : userMap.get(object.getInt("userid"));
                    String room = object.getString("room");

                    if (user == null) return;

                    String displayedMessage = I18n.format("gui.scrapchat.user_left_room", user.getUsername(), room);
                    scrapMessage(displayedMessage,  new Style().setColor(TextFormatting.GRAY).setItalic(true));
                }
                catch (Exception e) {
                    ScrapChat.logger.error("Error on user leaving room:\n" + e.getLocalizedMessage());
                }
            }
        };

        private Emitter.Listener onUserJoined = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject object = (JSONObject) args[0];
                    ChatUser user = object.has("user") ? new ChatUser(object.getJSONObject("user")) : userMap.get(object.getInt("userid"));

                    if (user == null) return;

                    String displayedMessage = I18n.format("gui.scrapchat.user_join", user.getUsername());
                    scrapMessage(displayedMessage,  new Style().setColor(TextFormatting.GRAY).setItalic(true));
                }
                catch (Exception e) {
                    ScrapChat.logger.error("Error on user joining chat:\n" + e.getLocalizedMessage());
                }
            }
        };

        private Emitter.Listener onUserLeft = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject object = (JSONObject) args[0];
                    ChatUser user = object.has("user") ? new ChatUser(object.getJSONObject("user")) : userMap.get(object.getInt("userid"));

                    if (user == null) return;

                    String displayedMessage = I18n.format("gui.scrapchat.user_left", user.getUsername());
                    scrapMessage(displayedMessage,  new Style().setColor(TextFormatting.GRAY).setItalic(true));
                }
                catch (Exception e) {
                    ScrapChat.logger.error("Error on user leaving chat:\n" + e.getLocalizedMessage());
                }
            }
        };

        private Emitter.Listener onMessage = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject object = (JSONObject) args[0];
                    String room = object.getString("room");
                    ChatUser user = null;
                    String message = object.getString("message");

                    // Single room mode filter
                    if (isSingleRoomMode()) {
                        if (!room.equals(currentRoom)) {
                            return;
                        }
                    }

                    // Turn User ID into a readable username
                    if (object.has("userid")) {
                        int userId = object.getInt("userid");
                        user = userMap.get(userId);
                    } else if (object.has("user")) { // Get User from included data
                        user = new ChatUser(object.getJSONObject("user"));
                    }

                    boolean action = object.has("action");
                    ChatMessage chatMessage = new ChatMessage(message, user, room, action);
                    scrapMessage(chatMessage.getFormattedMessage());
                }
                catch (Exception e) {
                    ScrapChat.logger.error("Error displaying message:\n" + e.getLocalizedMessage());
                }
            }
        };

        public Emitter.Listener onServer = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    String message = (String) args[0];
                    scrapMessage(message, new Style().setColor(TextFormatting.GRAY).setItalic(true));
                }
                catch (Exception e) {
                    ScrapChat.logger.error("Error displaying server message:\n" + e.getLocalizedMessage());
                }
            }
        };

        public Emitter.Listener onUserUpdate = new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject object = (JSONObject) args[0];
                    ChatUser user = new ChatUser(object.getJSONObject("user"));
                    userMap.put(user.getId(), user);
                }
                catch (Exception e) {
                    ScrapChat.logger.error("Error on updating user:\n" + e.getLocalizedMessage());
                }
            }
        };

    }

}
