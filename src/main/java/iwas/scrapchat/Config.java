package iwas.scrapchat;

import net.minecraftforge.common.config.Configuration;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.util.List;

public class Config {

    public static final String DEFAULT_TOKEN = "[TOKEN]";
    public static final String DEFAULT_CLIENT_NAME = "[CLIENTNAME]";
    public static final String DEFAULT_KEY = "[KEY]";

    public static String token;
    public static String clientName;
    public static String key;
    public static boolean showInGame;
    public static boolean addChatButtons;
    public static boolean suppressMCInSRM;

    public static void read() {
        Configuration cfg = ScrapChat.config;

        try {
            cfg.load();
            showInGame = cfg.get("general", "showInGame", true).getBoolean();
            addChatButtons = cfg.get("general", "addChatButtons", true).getBoolean();
            suppressMCInSRM = cfg.get("general", "suppressMCInSRM", true).getBoolean();

            token = cfg.get("data", "token", DEFAULT_TOKEN).getString();
            clientName = cfg.get("data", "clientname", DEFAULT_CLIENT_NAME).getString();
            key = cfg.get("data", "key", DEFAULT_KEY).getString();
        }
        catch (Exception e) {
            ScrapChat.logger.error("Error reading config:\n" + e.getLocalizedMessage());
        }
        finally {
            if (cfg.hasChanged()) cfg.save();
        }
    }

    public static void write() {
        ScrapChat.logger.info("Saving config...");

        Configuration cfg = ScrapChat.config;
        File file = cfg.getConfigFile();

        try {
            List<String> lines = Files.readAllLines(file.toPath());
            FileWriter writer = new FileWriter(file);

            for (int i = 0; i < lines.size(); i++) {
                // Edit lines
                String str = lines.get(i);
                str = str.replace(DEFAULT_TOKEN, token);
                str = str.replace(DEFAULT_CLIENT_NAME, clientName);
                str = str.replace(DEFAULT_KEY, key);

                // Output them
                writer.write(str + "\n");
            }

            // Now output
            writer.close();

        }
        catch (Exception e) {
            ScrapChat.logger.error("Error writing to config:\n" + e.getLocalizedMessage());
        }
    }

}
