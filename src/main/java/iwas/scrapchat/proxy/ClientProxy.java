package iwas.scrapchat.proxy;

import iwas.scrapchat.Config;
import iwas.scrapchat.command.ScrapCommand;
import iwas.scrapchat.command.ShortToggleCommand;
import iwas.scrapchat.event.RenderChatEvent;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod.EventBusSubscriber(Side.CLIENT)
public class ClientProxy extends CommonProxy{

    @SideOnly(Side.CLIENT)
    public void postInit(FMLPostInitializationEvent event) {
        ClientCommandHandler.instance.registerCommand(new ScrapCommand());
        ClientCommandHandler.instance.registerCommand(new ShortToggleCommand());

        if (Config.addChatButtons) MinecraftForge.EVENT_BUS.register(new RenderChatEvent()); // Add chat enhancements
    }

}
