package iwas.scrapchat.command;

import iwas.scrapchat.Config;
import iwas.scrapchat.ScrapChat;
import iwas.scrapchat.ScrapConnection;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import okhttp3.*;
import org.json.JSONObject;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class ScrapCommand extends CommandBase {

    @Override
    public String getName() {
        return "scrap";
    }

    @Override
    public List<String> getAliases() {
        List<String> aliases = new ArrayList<>();
        aliases.add("scrap");
        aliases.add("sc");
        return aliases;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "/scrap connect";
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        List<String> autoCompletes = new ArrayList<>();

        for (byte i = 0; i < args.length; i++) ScrapChat.logger.info(args[i]);

        if (args.length == 1) {
            autoCompletes.add("connect");
            autoCompletes.add("disconnect");
            autoCompletes.add("key");
            autoCompletes.add("join");
            autoCompletes.add("leave");
            autoCompletes.add("switch");
            autoCompletes.add("srm");
            autoCompletes.add("toggle");
            return autoCompletes;
        }

        switch (args[0]) {
            case "leave":
                autoCompletes.addAll(ScrapChat.chat.getInRooms()); // Add residing rooms to autocompletes
                break;
            case "switch":
                autoCompletes.addAll(ScrapChat.chat.getInRooms());
                break;
        }

        return autoCompletes;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        if (args.length == 0) {
            ScrapConnection.chatString(getUsage(sender));
            return;
        }

        switch (args[0]) {
            // Connect
            case "connect":
                // Need setup
                if (Config.token.equals(Config.DEFAULT_TOKEN)) {
                    ScrapConnection.chatString(I18n.format("gui.scrapchat.setup"), new Style().setColor(TextFormatting.AQUA));
                    return;
                }

                // Is chat active?
                if (ScrapChat.chat.isConnected()) {
                    ScrapConnection.chatString(I18n.format("gui.scrapchat.need_disconnection"), new Style().setColor(TextFormatting.AQUA));
                    return;
                }

                ScrapConnection.chatString(I18n.format("gui.scrapchat.connecting"), new Style().setColor(TextFormatting.AQUA));
                ScrapChat.chat.start();
                break;

            // Disconnect
            case "disconnect":

                // Is chat active?
                if (!ScrapChat.chat.isConnected()) {
                    ScrapConnection.chatString(I18n.format("gui.scrapchat.need_connection"), new Style().setColor(TextFormatting.AQUA));
                    return;
                }

                ScrapChat.chat.end();
                break;

            // Key
            case "key":
                if (args.length != 2) {
                    ScrapConnection.chatString("Usage: /scrap key [key]");
                    return;
                }

                Config.key = args[1];
                Config.clientName = Minecraft.getMinecraft().player.getName() + "@MinecraftForge";

                // Run thread to get client token
                GetCredentials task = new GetCredentials();
                Thread thread = new Thread(task);
                thread.start();
                break;

            // Show rooms
            case "rooms":
                if (args.length != 1) {
                    ScrapConnection.chatString("Usage: /scrap rooms");
                    return;
                }

                // Is chat active?
                if (!ScrapChat.chat.isConnected()) {
                    ScrapConnection.chatString(I18n.format("gui.scrapchat.need_connection"), new Style().setColor(TextFormatting.AQUA));
                    return;
                }

                String displayed = I18n.format("gui.scrapchat.rooms", ScrapChat.chat.getCurrentRoom(), ScrapChat.chat.getInRooms().toString());
                ScrapConnection.chatString(displayed, new Style().setColor(TextFormatting.AQUA));
                break;

            // Join
            case "join":
                if (args.length != 2) {
                    ScrapConnection.chatString("Usage: /scrap join [roomname]");
                    return;
                }

                // Is chat active?
                if (!ScrapChat.chat.isConnected()) {
                    ScrapConnection.chatString(I18n.format("gui.scrapchat.need_connection"), new Style().setColor(TextFormatting.AQUA));
                    return;
                }

                ScrapChat.chat.joinRoom(args[1]);
                break;

            // Leave
            case "leave":
                if (args.length != 2) {
                    ScrapConnection.chatString("Usage: /scrap leave [roomname]");
                    return;
                }

                // Is chat active?
                if (!ScrapChat.chat.isConnected()) {
                    ScrapConnection.chatString(I18n.format("gui.scrapchat.need_connection"), new Style().setColor(TextFormatting.AQUA));
                    return;
                }

                ScrapChat.chat.leaveRoom(args[1]);
                break;

            // Switch
            case "switch":
                if (args.length != 2) {
                    ScrapConnection.chatString("Usage: /scrap switch [roomname]");
                    return;
                }

                // Is chat active?
                if (!ScrapChat.chat.isConnected()) {
                    ScrapConnection.chatString(I18n.format("gui.scrapchat.need_connection"), new Style().setColor(TextFormatting.AQUA));
                    return;
                }

                ScrapChat.chat.switchRooms(args[1]);
                break;

            // Single Room Mode
            case "srm":
                if (args.length != 1) {
                    ScrapConnection.chatString("Usage: /scrap srm");
                    return;
                }

                ScrapChat.chat.toggleSingleRoomMode();
                break;

            // Toggle ScrapChat on/off
            case "toggle":
                if (args.length != 1) {
                    ScrapConnection.chatString("Usage: /scrap toggle");
                    return;
                }

                // Is chat active?
                if (!ScrapChat.chat.isConnected()) {
                    ScrapConnection.chatString(I18n.format("gui.scrapchat.need_connection"), new Style().setColor(TextFormatting.AQUA));
                    return;
                }

                ScrapChat.chat.toggleActive();
                break;
        }
    }

    private static class GetCredentials implements Runnable {

        @Override
        public void run() {
            try {
                OkHttpClient client = new OkHttpClient();

                RequestBody body = new FormBody.Builder()
                        .add("key", Config.key)
                        .add("client_id", "forge_scrap_chat")
                        .add("client_name", Config.clientName)
                        .build();

                Request request = new Request.Builder()
                        .url("https://dev.scrap.tf/api/user/login.php")
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();

                // Pauses here, waits for response...

                String result = response.body().string();
                JSONObject object = new JSONObject(result);

                // Success!
                if (object.getBoolean("success")) {
                    ScrapConnection.chatString(I18n.format("gui.scrapchat.key_activated"), new Style().setColor(TextFormatting.AQUA));
                    Config.token = object.getString("token");
                    Config.write(); // Save config

                } else {
                    ScrapConnection.chatString(I18n.format("gui.scrapchat.key_error"), new Style().setColor(TextFormatting.AQUA));
                    ScrapConnection.chatString(object.getString("message"), new Style().setColor(TextFormatting.AQUA).setItalic(true));
                }

            }
            catch (Exception e) {
                ScrapChat.logger.error("Error in getting login credentials:\n" + e.getLocalizedMessage());
            }

        }
    }

}
