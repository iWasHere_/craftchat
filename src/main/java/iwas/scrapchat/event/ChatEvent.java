package iwas.scrapchat.event;

import iwas.scrapchat.Config;
import iwas.scrapchat.ScrapChat;
import net.minecraft.util.text.ChatType;
import net.minecraftforge.client.event.ClientChatEvent;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber
public class ChatEvent {

    @SubscribeEvent
    public static void chat(ClientChatEvent event) {
        String message = event.getMessage();
        if (!ScrapChat.chat.isActive()) return;

        if (message.startsWith("/") && !message.startsWith("//")) return; // Don't capture commands
        if (message.startsWith("/me")) return; // Don't capture /me

        if (ScrapChat.chat.isConnected()) {
            ScrapChat.chat.sendMessage(message);
            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public static void receive(ClientChatReceivedEvent event) {
        // Hide non-Scrap messages when in a room in Single Room Mode
        if (!Config.suppressMCInSRM) return;

        if (ScrapChat.chat.isActive() && ScrapChat.chat.isSingleRoomMode()) {
            if (event.getType().equals(ChatType.CHAT)) {
                event.setCanceled(true);
            }
        }
    }

}
