package iwas.scrapchat;

import iwas.scrapchat.proxy.CommonProxy;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

import java.io.File;

@Mod(modid = ScrapChat.MODID, name = ScrapChat.NAME, version = ScrapChat.VERSION)
public class ScrapChat
{
    public static final String MODID = "scrapchat";
    public static final String NAME = "Scrap Chat";
    public static final String VERSION = "1.0";

    public static Logger logger;
    public static Configuration config;

    public static ScrapConnection chat;

    @SidedProxy(clientSide = "iwas.scrapchat.proxy.ClientProxy")
    public static CommonProxy proxy;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        logger = event.getModLog();

        // Config
        config = new Configuration(new File(event.getModConfigurationDirectory(), "scrapchat.cfg"));
        Config.read();

        // Connection
        chat = new ScrapConnection();
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
        if (config.hasChanged()) config.save();
        // Command
        proxy.postInit(event);
    }
}
